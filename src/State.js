import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import Header from "./components/header/Header";
import MainLayout from "./layouts/MainLayout";
import Button from "./components/button/Button";

function State() {
  return (
    <div className="State mt-5">
      <Header title="Button Components" />

      <MainLayout>
        <Button variant="primary">Tes 1</Button>

        <Button variant="succes">Tes 2</Button>

        <Button variant="black">Tes 3</Button>
      </MainLayout>
    </div>
  );
}

export default State;
